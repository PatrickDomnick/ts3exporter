package collector

import (
	"log"
	"strconv"

	"github.com/hikhvar/ts3exporter/pkg/serverquery"
	"github.com/prometheus/client_golang/prometheus"
)

const channelSubsystem = "channel"

var channelLabels = []string{virtualServerLabel, channelLabel}
var clientLabels = []string{virtualServerLabel, channelLabel, clientId, clientName, clientVersion, clientPlatform}

type Channel struct {
	executor        serverquery.Executor
	internalMetrics *ExporterMetrics

	ClientsOnline        *prometheus.Desc
	ClientsTalking       *prometheus.Desc
	ClientsInputMuted    *prometheus.Desc
	ClientsOutputMuted   *prometheus.Desc
	ClientsIdleTime      *prometheus.Desc
	ClientsLastConnected *prometheus.Desc
	MaxClients           *prometheus.Desc
	Codec                *prometheus.Desc
	CodecQuality         *prometheus.Desc
	LatencyFactor        *prometheus.Desc
	Unencrypted          *prometheus.Desc
	Permanent            *prometheus.Desc
	SemiPermanent        *prometheus.Desc
	Default              *prometheus.Desc
	Password             *prometheus.Desc
	User                 *prometheus.Desc
}

func NewChannel(executor serverquery.Executor, internalMetrics *ExporterMetrics) *Channel {
	return &Channel{
		executor:             executor,
		internalMetrics:      internalMetrics,
		ClientsOnline:        prometheus.NewDesc(fqdn(channelSubsystem, "clients_online"), "number of clients currently online", clientLabels, nil),
		ClientsTalking:       prometheus.NewDesc(fqdn(channelSubsystem, "clients_talking"), "number of clients currently talking", clientLabels, nil),
		ClientsInputMuted:    prometheus.NewDesc(fqdn(channelSubsystem, "clients_input_muted"), "number of clients currently input muted", clientLabels, nil),
		ClientsOutputMuted:   prometheus.NewDesc(fqdn(channelSubsystem, "clients_output_muted"), "number of clients currently output muted", clientLabels, nil),
		ClientsIdleTime:      prometheus.NewDesc(fqdn(channelSubsystem, "clients_idle_time"), "number seconds of clients idle", clientLabels, nil),
		ClientsLastConnected: prometheus.NewDesc(fqdn(channelSubsystem, "clients_last_connected"), "timestamp of clients last connection", clientLabels, nil),
		MaxClients:           prometheus.NewDesc(fqdn(channelSubsystem, "max_clients"), "maximal number of clients allowed in this channel", channelLabels, nil),
		Codec:                prometheus.NewDesc(fqdn(channelSubsystem, "codec"), "numeric number of configured codec for this channel", channelLabels, nil),
		CodecQuality:         prometheus.NewDesc(fqdn(channelSubsystem, "codec_quality"), "numeric number of codec quality level chosen for this channel", channelLabels, nil),
		LatencyFactor:        prometheus.NewDesc(fqdn(channelSubsystem, "codec_latency_factor"), "numeric number of codec latency factor chosen for this channel", channelLabels, nil),
		Unencrypted:          prometheus.NewDesc(fqdn(channelSubsystem, "unencrypted"), "is the channel unencrypted", channelLabels, nil),
		Permanent:            prometheus.NewDesc(fqdn(channelSubsystem, "permanent"), "is the channel permanent", channelLabels, nil),
		SemiPermanent:        prometheus.NewDesc(fqdn(channelSubsystem, "semi_permanent"), "is the channel semi permanent", channelLabels, nil),
		Default:              prometheus.NewDesc(fqdn(channelSubsystem, "default"), "is the channel the default channel", channelLabels, nil),
		Password:             prometheus.NewDesc(fqdn(channelSubsystem, "password"), "is the channel saved by an password", channelLabels, nil),
	}
}

func (c *Channel) Describe(desc chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(c, desc)
}

func (c *Channel) Collect(met chan<- prometheus.Metric) {
	channels := serverquery.NewChannelView(c.executor)
	if err := channels.Refresh(); err != nil {
		c.internalMetrics.RefreshError(channelSubsystem)
		log.Printf("failed to refresh channel view: %v", err)
	}
	for _, ch := range channels.All() {
		// ts3_channel_clients_online{channel="Channel 1",clientid="",clientname="",virtualserver="Serva"} 0
		// ts3_channel_clients_online{channel="Channel M",clientid="1",clientname="Padde",virtualserver="Serva"} 1
		// ts3_channel_clients_online{channel="Channel M",clientid="2",clientname="Henry",virtualserver="Serva"} 1
		// ts3_channel_clients_online{channel="Channel X",clientid="",clientname="",virtualserver="Serva"} 0
		if len(ch.Clients) == 0 {
			met <- prometheus.MustNewConstMetric(c.ClientsOnline, prometheus.GaugeValue, float64(0), ch.HostingServer.Name, ch.Name, "", "", "", "")
		} else {
			for _, tsClient := range ch.Clients {
				met <- prometheus.MustNewConstMetric(c.ClientsOnline, prometheus.GaugeValue, float64(1), ch.HostingServer.Name, ch.Name, strconv.Itoa(tsClient.ID), tsClient.Nickname, tsClient.Version, tsClient.Platform)
				met <- prometheus.MustNewConstMetric(c.ClientsTalking, prometheus.GaugeValue, float64(tsClient.Talking), ch.HostingServer.Name, ch.Name, strconv.Itoa(tsClient.ID), tsClient.Nickname, tsClient.Version, tsClient.Platform)
				met <- prometheus.MustNewConstMetric(c.ClientsInputMuted, prometheus.GaugeValue, float64(tsClient.InputMuted), ch.HostingServer.Name, ch.Name, strconv.Itoa(tsClient.ID), tsClient.Nickname, tsClient.Version, tsClient.Platform)
				met <- prometheus.MustNewConstMetric(c.ClientsOutputMuted, prometheus.GaugeValue, float64(tsClient.OutputMuted), ch.HostingServer.Name, ch.Name, strconv.Itoa(tsClient.ID), tsClient.Nickname, tsClient.Version, tsClient.Platform)
				met <- prometheus.MustNewConstMetric(c.ClientsIdleTime, prometheus.GaugeValue, float64(tsClient.IdleTime), ch.HostingServer.Name, ch.Name, strconv.Itoa(tsClient.ID), tsClient.Nickname, tsClient.Version, tsClient.Platform)
				met <- prometheus.MustNewConstMetric(c.ClientsLastConnected, prometheus.GaugeValue, float64(tsClient.LastConnected), ch.HostingServer.Name, ch.Name, strconv.Itoa(tsClient.ID), tsClient.Nickname, tsClient.Version, tsClient.Platform)
			}
		}
		met <- prometheus.MustNewConstMetric(c.MaxClients, prometheus.GaugeValue, float64(ch.MaxClients), ch.HostingServer.Name, ch.Name)
		met <- prometheus.MustNewConstMetric(c.Codec, prometheus.GaugeValue, float64(ch.Codec), ch.HostingServer.Name, ch.Name)
		met <- prometheus.MustNewConstMetric(c.CodecQuality, prometheus.GaugeValue, float64(ch.CodecQuality), ch.HostingServer.Name, ch.Name)
		met <- prometheus.MustNewConstMetric(c.LatencyFactor, prometheus.GaugeValue, float64(ch.LatencyFactor), ch.HostingServer.Name, ch.Name)
		met <- prometheus.MustNewConstMetric(c.Unencrypted, prometheus.GaugeValue, float64(ch.Unencrypted), ch.HostingServer.Name, ch.Name)
		met <- prometheus.MustNewConstMetric(c.Permanent, prometheus.GaugeValue, float64(ch.Permanent), ch.HostingServer.Name, ch.Name)
		met <- prometheus.MustNewConstMetric(c.SemiPermanent, prometheus.GaugeValue, float64(ch.SemiPermanent), ch.HostingServer.Name, ch.Name)
		met <- prometheus.MustNewConstMetric(c.Default, prometheus.GaugeValue, float64(ch.Default), ch.HostingServer.Name, ch.Name)
		met <- prometheus.MustNewConstMetric(c.Password, prometheus.GaugeValue, float64(ch.Password), ch.HostingServer.Name, ch.Name)
	}
}
