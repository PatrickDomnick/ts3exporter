package serverquery

import "fmt"

type ChannelID int

type TeamspeakClient struct {
	ID            int       `sq:"clid"`
	ChannelID     ChannelID `sq:"cid"`
	DID           int       `sq:"client_database_id"`
	Nickname      string    `sq:"client_nickname"`
	Type          int       `sq:"client_type"`
	Talking       int       `sq:"client_flag_talking"`
	InputMuted    int       `sq:"client_input_muted"`
	OutputMuted   int       `sq:"client_output_muted"`
	IdleTime      int       `sq:"client_idle_time"`
	LastConnected int       `sq:"client_lastconnected"`
	Version       string    `sq:"client_version"`
	Platform      string    `sq:"client_platform"`
}

type Channel struct {
	HostingServer VirtualServer
	ID            ChannelID `sq:"cid"`
	PID           int       `sq:"pid"`
	Order         int       `sq:"channel_order"`
	Name          string    `sq:"channel_name"`
	ClientsOnline int       `sq:"total_clients"`
	MaxClients    int       `sq:"channel_maxclients"`
	Codec         int       `sq:"channel_codec"`
	CodecQuality  int       `sq:"channel_codec_quality"`
	LatencyFactor int       `sq:"channel_codec_latency_factor"`
	Unencrypted   int       `sq:"channel_codec_is_unencrypted"`
	Permanent     int       `sq:"channel_flag_permanent"`
	SemiPermanent int       `sq:"channel_flag_semi_permanent"`
	Default       int       `sq:"channel_flag_default"`
	Password      int       `sq:"channel_flag_password"`
	Clients       []TeamspeakClient
}

type ChannelView struct {
	e                Executor
	channels         map[ChannelID]Channel
	vServerInventory *VirtualServerView
}

func NewChannelView(e Executor) *ChannelView {
	return &ChannelView{
		e:                e,
		channels:         make(map[ChannelID]Channel),
		vServerInventory: NewVirtualServer(e),
	}
}

// Refresh refreshes the internal representation of the ChannelView. It changes into all virtual server
// known by the vServer inventory.
func (c *ChannelView) Refresh() error {
	if err := c.vServerInventory.Refresh(); err != nil {
		return fmt.Errorf("failed to update vserver inventory: %w", err)
	}
	for _, vServer := range c.vServerInventory.All() {
		err := c.updateAllOnVServer(vServer)
		if err != nil {
			return fmt.Errorf("failed to update metrics on vServer %s: %w", vServer.Name, err)
		}
	}
	return nil
}

// All returns all known Channels
func (c *ChannelView) All() []Channel {
	ret := make([]Channel, 0, len(c.channels))
	for _, ch := range c.channels {
		ret = append(ret, ch)
	}
	return ret
}

// updateAllOnVServer update all channels on the given virtual server
func (c *ChannelView) updateAllOnVServer(vServer VirtualServer) error {
	_, err := c.e.Exec(fmt.Sprintf("use %d", vServer.ID))
	if err != nil {
		return fmt.Errorf("failed to use virtual server %d: %w", vServer.ID, err)
	}
	res, err := c.e.Exec("channellist")
	if err != nil {
		return fmt.Errorf("failed to list channels: %w", err)
	}
	for _, r := range res {
		for _, i := range r.Items {
			var ch Channel
			if err := i.ReadInto(&ch); err != nil {
				return fmt.Errorf("failed to parse channel from response: %w", err)
			}
			if err := c.getDetails(&ch); err != nil {
				return fmt.Errorf("failed to parse details for channel %d: %w", ch.ID, err)
			}
			ch.HostingServer = vServer
			// Clients
			client_res, err := c.e.Exec("clientlist -voice -times -info")
			var cli TeamspeakClient
			if err != nil {
				return fmt.Errorf("failed to get clients")
			}
			for _, rc := range client_res {
				for _, ic := range rc.Items {
					if err := ic.ReadInto(&cli); err != nil {
						return fmt.Errorf("failed to parse clients from response: %w", err)
					}
					if cli.ChannelID == ch.ID {
						if cli.Type == 0 {
							ch.Clients = append(ch.Clients, cli)
						}
					}
				}
			}
			c.channels[ch.ID] = ch
		}
	}
	return nil
}

// getDetails populates the given channels with details
func (c *ChannelView) getDetails(ch *Channel) error {
	res, err := c.e.Exec(fmt.Sprintf("channelinfo cid=%d", ch.ID))
	if err != nil {
		return fmt.Errorf("failed to run channelinfo command: %w", err)
	}
	if len(res) < 1 {
		return fmt.Errorf("expected at least one response line")
	}
	if len(res[0].Items) != 1 {
		return fmt.Errorf("expected exactly one channelinfo response got %d", len(res[0].Items))
	}
	if err = res[0].Items[0].ReadInto(ch); err != nil {
		return fmt.Errorf("failed to parse channel response: %w", err)
	}
	return nil

}
