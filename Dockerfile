FROM golang:1.16.6-alpine3.14 AS build

ADD . /go/src/github.com/hikhvar/ts3exporter

RUN cd /go/src/github.com/hikhvar/ts3exporter && \
    go get -d -v ./... && \
    CGO_ENABLED=0 go build -o /go/bin/ts3exporter

FROM scratch

COPY --from=build /go/bin/ts3exporter /

EXPOSE 9189/tcp
ENTRYPOINT ["/ts3exporter"]
